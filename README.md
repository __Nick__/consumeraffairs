# Consumer Affairs challenge

## Guidelines

It's a simple task that is similar to a real-world assignment on the team and shouldn't take more than 2-3 hours
They can complete the task before or after the interview

​Backend

Using Django, create a simple API that allows users to post and retrieve their reviews.

Acceptance Criteria
Users are able to submit reviews to the API
Users are able to retrieve reviews that they submitted
Users cannot see reviews submitted by other users
Use of the API requires a unique auth token for each user
Submitted reviews must include, at least, the following attributes:
Rating - must be between 1 - 5
Title - no more than 64 chars
Summary - no more than 10k chars
IP Address - IP of the review submitter
Submission date - the date the review was submitted
Company - information about the company for which the review was submitted, can be simple text (e.g., name, company id, etc.) or a separate model altogether
Reviewer Metadata - information about the reviewer, can be simple text em(e.g., name, email, reviewer id, etc.) or a separate model altogether

Optional:
Provide an authenticated admin view that allows me to view review submissions

Organize the schema and data models in whatever manner you think makes the most sense and feel free to add any additional style and flair to the project that you'd like.​


## HOW TO

execute the following commands in order to have the system up and running:

```sh

$ ./manage.py migrate
$ ./manage.py runserver

```

The migration process will create a user with the following credentials:

__user:__ testuser
__pass:__ consumeraffairs
