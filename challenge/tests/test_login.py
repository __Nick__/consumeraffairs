from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory
from django.urls import reverse
from django.contrib import auth


class LogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser2',
            'password': 'consumeraffairs'}
        self.invalid_credentials = {'username': 'invalid',
                                    'password': 'invalid'}
        self.factory = RequestFactory()
        self.user = User.objects.create_user(**self.credentials)

    def test_login(self):
        self.assertTrue(self.client.login(**self.credentials))

    def test_redirect_login(self):
        path = '/reviews/list'
        request = self.factory.get('/')
        self.user = User.objects.create_user(**self.invalid_credentials)
        request.user = self.user
        response = self.client.get(path, follow=False)
        # should redirect
        self.assertEqual(response.status_code, 302)
        self.assertEqual('/?next=' + path, response.url)
