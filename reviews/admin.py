from django.contrib import admin
from .models import Review, Company
# Register your models here.


admin.register(Review)(admin.ModelAdmin)
admin.register(Company)(admin.ModelAdmin)