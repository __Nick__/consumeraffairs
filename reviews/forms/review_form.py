from django.forms import (
    ModelForm,
    CharField,
    IntegerField,
    Textarea
)
from django.core.validators import (
    MaxValueValidator,
    MinValueValidator,
    MaxLengthValidator
)
from ..models import Review, Company


class ReviewForm(ModelForm):
    company = CharField()
    summary = CharField(widget=Textarea,
                        validators=[MaxLengthValidator(10000)])
    rating = IntegerField(validators=[MaxValueValidator(5),
                                      MinValueValidator(1)])
    title = CharField(validators=[MaxLengthValidator(64)])
    exclude = ['reviewer']

    class Meta:
        model = Review
        # series = ModelChoiceField(queryset=Company.objects.all())
        fields = ['title', 'rating', 'summary', 'company']
        
    def clean(self):
        company, created = Company.objects.get_or_create(
            name=self.cleaned_data['company']
        )
        self.cleaned_data['company'] = company

    def save(self, commit=True):
        company, created = Company.objects.get_or_create(
            name=self.cleaned_data['company']
        )
        # self.cleaned_data['reviewer'] = user
        self.cleaned_data['company'] = company
        return super(ReviewForm, self).save(commit)
