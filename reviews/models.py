from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


class Company(models.Model):

    name = models.CharField(max_length=40)
    email = models.CharField(max_length=40)
    phone = models.CharField(max_length=50)


class Review(models.Model):

    rating = models.IntegerField(default=1)
    title = models.CharField(max_length=64, blank=False, null=False)
    summary = models.TextField()
    submission_date = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey(Company, related_name='company_name',
                                on_delete=models.CASCADE)
    reviewer = models.ForeignKey(User, on_delete=models.CASCADE)
    reviewers_ip = models.CharField(max_length=16)
