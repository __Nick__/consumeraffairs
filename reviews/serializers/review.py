from ..models import Review, Company


class ReviewSerializer:

    @classmethod
    def serialize(self, reviews):
        reviews_repr = []
        for review in reviews:
            review_repr = dict(company='', rating= 1, title='', summary='')
            review_repr['company'] = Company.objects.get(pk=review.company_id).name
            review_repr['rating'] = review.rating
            review_repr['title'] = review.title
            review_repr['rating'] = review.rating
            review_repr['summary'] = review.summary
            review_repr['submission_date'] = review.submission_date

            reviews_repr.append(review_repr)
        return reviews_repr
