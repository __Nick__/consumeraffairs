from django.test import TestCase
from reviews.forms.review_form import ReviewForm


class ReviewFormTest(TestCase):

    def setUp(self):
        pass

    def test_empty_title(self):
        data = {'title': '', 'rating': 1, 'summary': 'test', 'company': 1}
        form = ReviewForm(data)
        self.assertFalse(form.is_valid())
        self.assertTrue('This field is required' in form.errors['title'][0])

    def test_title_larget_than_max(self):
        title = 'a' * 65
        data = {'title': title, 'rating': 1, 'summary': 'test', 'company': 1}
        form = ReviewForm(data)
        self.assertFalse(form.is_valid())
        self.assertTrue('has at most 64 characters' in form.errors['title'][0])

    def test_negative_rating(self):
        data = {'title': 'test', 'rating': -1, 'summary': 'test', 'company': 1}
        form = ReviewForm(data)
        self.assertFalse(form.is_valid())
        self.assertTrue(
            'greater than or equal to 1' in form.errors['rating'][0])

    def test_larger_rating(self):
        data = {'title': 'test', 'rating': 6, 'summary': 'test', 'company': 1}
        form = ReviewForm(data)
        self.assertFalse(form.is_valid())
        self.assertTrue('less than or equal to 5' in form.errors['rating'][0])

    def test_larger_than_max_summary(self):
        summary = 'a' * 10001
        data = {'title': 'test', 'rating': 1, 'summary': summary, 'company': 1}
        form = ReviewForm(data)
        self.assertFalse(form.is_valid())
        self.assertTrue(
            'has at most 10000 characters' in form.errors['summary'][0])

    # def test_invalid_title(self):
    #     data = {'title': '', 'rating':1, 'summary':'test', 'company': 1}
    #     form = ReviewForm(data)
    #     self.assertFalse(form.is_valid())
    #     self.assertTrue( 'This field is required' in form.errors['Title'])

    # def test_invalid_title(self):
    #     data = {'title': '', 'rating':1, 'summary':'test', 'company': 1}
    #     form = ReviewForm(data)
    #     self.assertFalse(form.is_valid())
    #     self.assertTrue( 'This field is required' in form.errors['Title'])
