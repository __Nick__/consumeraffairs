from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory


class ReviewListTest(TestCase):

    fixtures = ['dump']

    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'consumeraffairs'}

    def test_not_logged_in_request(self):
        resp = self.client.get('/reviews/list')
        self.assertEqual(resp.status_code, 302)

    def test_valid_request(self):
        self.client.login(**self.credentials)
        resp = self.client.get('/reviews/list')
        self.assertEqual(resp.status_code, 200)
