from .views.review import ReviewList, Review
from django.conf.urls import url



urlpatterns = (
    url(r'list$', ReviewList.as_view(), name='review.review_list'),
    url(r'review$', Review.as_view(), name='review.reviews'),
   
)
