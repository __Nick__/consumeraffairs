from django.shortcuts import render
from django.views.generic import View
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
import django_tables2 as tables
from django_tables2 import RequestConfig
from ipware.ip import get_ip
from ..models import Review as ReviewModel
from ..forms.review_form import ReviewForm

from ..serializers.review import ReviewSerializer


class SimpleTable(tables.Table):
    company = tables.Column()
    rating = tables.Column()
    title = tables.Column()
    summary = tables.Column()
    submission_date = tables.DateTimeColumn()

    class Meta:
        attrs = {'class': 'table table-bordered table-striped table-hover'}


class ReviewList(LoginRequiredMixin, View):

    def get(self, request):
        queryset = ReviewModel.objects.filter(reviewer=request.user)
        reviews = ReviewSerializer.serialize(queryset)
        table = SimpleTable(reviews)
        RequestConfig(request).configure(table)
        table.paginate(page=request.GET.get('page', 1), per_page=5)
        return render(request, 'review_list.html', {'table': table})


class Review(LoginRequiredMixin, FormView):
    template_name = 'review.html'
    form_class = ReviewForm
    success_url = '/reviews/list'

    def form_valid(self, form):
        if form.is_valid():
            review = form.save(commit=False)
            review.reviewer = self.request.user
            review.reviewers_ip = get_ip(self.request)
            review.save()
        return super().form_valid(form)
